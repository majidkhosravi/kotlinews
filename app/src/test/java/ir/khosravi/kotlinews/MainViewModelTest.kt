package ir.khosravi.kotlinews

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import ir.khosravi.kotlinews.model.ResponseModel
import ir.khosravi.kotlinews.viewModel.MainViewModel
import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.ArgumentCaptor
import org.mockito.Mock
import org.mockito.Mockito.times
import org.mockito.Mockito.verify
import org.mockito.MockitoAnnotations
import java.util.logging.Handler


@RunWith(JUnit4::class)
class MainViewModelTest {

    @get:Rule
    val rule = InstantTaskExecutorRule()

    @Mock
    private lateinit var viewModel: MainViewModel

//    private val responseModelObserver: Observer<ResponseModel> =
//        mock(Observer::class.java) as Observer<ResponseModel>

    @Mock
    lateinit var responseModelObserver: Observer<ResponseModel>

    @Mock
    lateinit var categoryObserver: Observer<String>

    @Mock
    lateinit var bottomSheetStateObserver: Observer<Int>

    @Before
    fun before() {
        MockitoAnnotations.initMocks(this)
        viewModel = MainViewModel()
//        viewModel.getResponseModel().observeForever(responseModelObserver)
        viewModel.getDefaultCategory().observeForever(categoryObserver)
        viewModel.getBottomSheetState().observeForever(bottomSheetStateObserver)

    }

    @Synchronized
    @Test
    fun fetchNewsList_ShouldReturnResponse() {
//        viewModel.fetchNewsList("", 1)

        val thread =  Thread()
        thread.run{
            val captor = ArgumentCaptor.forClass(ResponseModel::class.java)
            captor.run {
                verify(responseModelObserver, times(2)).onChanged(capture())
                assertEquals(value.status, "ok")
                //          assertEquals(expectedUser, value)
            }
        }
    }

    @Test
    fun defaultCategory_ShouldReturnAll() {
        val captor = ArgumentCaptor.forClass(String::class.java)
        captor.run {
            verify(categoryObserver, times(1)).onChanged(capture())
            assertNotNull(value)
            assertNotEquals(value, "")
//            assertEquals(value, "All")
            assertEquals(value, "Business")
        }
    }

    @Test
    fun bottomSheetState_ShouldReturnHide() {
        val captor = ArgumentCaptor.forClass(Int::class.java)
        captor.run {
            verify(bottomSheetStateObserver, times(1)).onChanged(capture())
            assertNotNull(value)
            assertNotEquals(value, "")
            assertEquals(value, MainViewModel.BottomSheetState.HIDE)
        }
    }

}



