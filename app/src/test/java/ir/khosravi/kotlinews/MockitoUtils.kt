package ir.khosravi.kotlinews

import org.mockito.Mockito

class MockitoUtils {

    companion object {
        inline fun <reified T> mock(): T = Mockito.mock(T::class.java)
    }

}