package ir.khosravi.kotlinews

import ir.khosravi.kotlinews.utils.DateUtils
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotEquals
import org.junit.Test

class DateUtilsTest {

    private val sampleCorrectInputDate: String = "2019-09-28T09:05:23Z"
    private val sampleCorrectDetailDateFormat: String = "09:05 - 28 Sep 2019"
    private val sampleCorrectItemDateFormat: String = "28 Sep 2019"


    @Test
    fun dateUtils_CorrectInput_DetailDate_ReturnCorrect() {
        assertEquals(DateUtils.getDetailDate(sampleCorrectInputDate), sampleCorrectDetailDateFormat)
    }

    @Test
    fun dateUtils_IncorrectInput_DetailDate_ReturnIncorrect() {
        assertNotEquals(DateUtils.getDetailDate(""), sampleCorrectDetailDateFormat)
    }

    @Test
    fun dateUtils_IncorrectInput_DetailDate_ReturnIncorrect2() {
        assertEquals(DateUtils.getDetailDate("2019-09-28"), "")
    }

    @Test
    fun dateUtils_IncorrectInput_DetailDate_ReturnCorrect() {
        assertNotEquals(DateUtils.getDetailDate("2019-09-28"), sampleCorrectDetailDateFormat)
    }

    @Test
    fun dateUtils_CorrectInput_ItemDate_ReturnCorrect() {
        assertEquals(DateUtils.getItemsDate(sampleCorrectInputDate), sampleCorrectItemDateFormat)
    }


}