package ir.khosravi.kotlinews.controller.paging

import android.view.ViewGroup
import androidx.paging.PagedListAdapter
import ir.khosravi.kotlinews.controller.BaseAdapter
import ir.khosravi.kotlinews.interfaces.OnItemClickListener
import ir.khosravi.kotlinews.model.NewsModel
import ir.khosravi.kotlinews.widget.NewsItemView

class NewsPagingAdapter :
    PagedListAdapter<NewsModel, BaseAdapter.BaseViewHolder>(DiffUtilCallBack()) {

    lateinit var itemClickListener: OnItemClickListener

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseAdapter.BaseViewHolder =
        BaseAdapter.BaseViewHolder(NewsItemView())

    override fun onBindViewHolder(holder: BaseAdapter.BaseViewHolder, position: Int) {
        if (holder.itemView is NewsItemView) holder.itemView.newsModel = getItem(position)

        if (::itemClickListener.isInitialized) {
            holder.itemView.setOnClickListener {
                itemClickListener.onItemClick(getItem(position))
            }
        }
    }
}
