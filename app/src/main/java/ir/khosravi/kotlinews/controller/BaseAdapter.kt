package ir.khosravi.kotlinews.controller

import android.view.ViewGroup
import ir.khosravi.kotlinews.interfaces.OnItemClickListener
import ir.khosravi.kotlinews.widget.BaseItemView

/**
 * © Author Majid Khosravi since 09/21/2019.
 * https://gitlab.com/majidkhosravi
 * majid.khosravi89@gmail.com
 * https://www.farakav.com
 */


/**
 * This class is an abstract RecyclerAdapter for take two master DataType
 * and generate basic adapter that should be parent of the other adapters.
 * This adapter has a {@link OnItemClickListener} will return click event to Activity or Fragment.
 * In this class, all ItemView must be subclass of the {@link BaseItemView}
 *
 */

abstract class BaseAdapter<DataClass, ViewClass : BaseItemView>(
    private val modelList: ArrayList<DataClass>,
    private val viewClass: Class<ViewClass>
) : androidx.recyclerview.widget.RecyclerView.Adapter<BaseAdapter.BaseViewHolder>() {

    lateinit var itemClickListener: OnItemClickListener

    override fun getItemCount(): Int = modelList.size

    override fun onCreateViewHolder(parent: ViewGroup, parentType: Int): BaseViewHolder =
        BaseViewHolder(viewClass.newInstance() as BaseItemView)

    override fun onBindViewHolder(viewHolder: BaseViewHolder, position: Int) =
        viewHolder.itemView.setOnClickListener {
            itemClickListener.onItemClick(modelList[position])
        }

    class BaseViewHolder(itemView: BaseItemView) :
        androidx.recyclerview.widget.RecyclerView.ViewHolder(itemView)

}


