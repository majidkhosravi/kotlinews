package ir.khosravi.kotlinews.controller.paging

import android.util.Log
import androidx.paging.PageKeyedDataSource
import ir.khosravi.kotlinews.api.ApiClient
import ir.khosravi.kotlinews.api.ApiInterface
import ir.khosravi.kotlinews.api.NetworkUtils
import ir.khosravi.kotlinews.model.NewsModel
import ir.khosravi.kotlinews.model.ResponseModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import retrofit2.Response

class NewsListDataSource(
    var scope: CoroutineScope,
    var category: String
) :
    PageKeyedDataSource<Int, NewsModel>() {
    private val firstPage = 1
    private lateinit var apiInterface: ApiInterface


    override fun loadInitial(
        params: LoadInitialParams<Int>,
        callback: LoadInitialCallback<Int, NewsModel>
    ) {
        scope.launch {
            try {
                val response: Response<ResponseModel> =
                    getApi().getNewsAsync(category = category, page = firstPage).await()
                when {
                    response.isSuccessful -> {
                        callback.onResult(response.body()!!.newsList, null, firstPage + 1)
                    }
                }
            } catch (e: Exception) {
                Log.e("Coroutine Exception", e.message)
                // TODO: Handle Error
            }
        }
    }


    override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<Int, NewsModel>) {
        scope.launch {
            try {
                val response: Response<ResponseModel> =
                    getApi().getNewsAsync(category = category, page = params.key).await()
                when {
                    response.isSuccessful -> {
                        val key: Int? = if (params.key > 1) params.key - 1 else null
                        callback.onResult(response.body()!!.newsList, key)
                    }
                }
            } catch (e: Exception) {
                Log.e("Coroutine Exception", e.message)
                // TODO: Handle Error
            }
        }
    }

    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Int, NewsModel>) {
        scope.launch {
            try {
                val response: Response<ResponseModel> =
                    getApi().getNewsAsync(category = category, page = params.key).await()
                when {
                    response.isSuccessful -> {
                        val hasMore =
                            response.body()!!.totalResults - params.key *
                                    NetworkUtils.DEFAULT_PAGE_SIZE >= 1
                        val key = if (hasMore) params.key + 1 else null
                        callback.onResult(response.body()!!.newsList, key)
                    }
                }
            } catch (e: Exception) {
                Log.e("Coroutine Exception", e.message)
                // TODO: Handle Error
            }
        }
    }

    private fun getApi(): ApiInterface {
        if (!::apiInterface.isInitialized) {
            apiInterface = ApiClient.getRetrofit().create(ApiInterface::class.java)
        }
        return apiInterface
    }

}