package ir.khosravi.kotlinews.controller.paging

import androidx.recyclerview.widget.DiffUtil
import ir.khosravi.kotlinews.model.NewsModel

class DiffUtilCallBack : DiffUtil.ItemCallback<NewsModel>() {

    override fun areItemsTheSame(oldItem: NewsModel, newItem: NewsModel): Boolean =
        oldItem.title.equals(newItem.title)

    override fun areContentsTheSame(oldItem: NewsModel, newItem: NewsModel): Boolean =
        oldItem == newItem

}