package ir.khosravi.kotlinews.controller

import ir.khosravi.kotlinews.model.NewsModel
import ir.khosravi.kotlinews.widget.NewsItemView

/**
 * © Author Majid Khosravi since 09/21/2019.
 * https://gitlab.com/majidkhosravi
 * majid.khosravi89@gmail.com
 * https://www.farakav.com
 */


class NewsAdapter(private val newsList: ArrayList<NewsModel>?) :
    BaseAdapter<NewsModel, NewsItemView>(newsList!!, NewsItemView::class.java) {

    override fun onBindViewHolder(viewHolder: BaseViewHolder, position: Int) {
        super.onBindViewHolder(viewHolder, position)
        val newsItemView = viewHolder.itemView as NewsItemView

        newsItemView.newsModel = newsList?.get(position)
    }
}