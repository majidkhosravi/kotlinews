package ir.khosravi.kotlinews.model

import com.google.gson.annotations.SerializedName

data class ResponseModel(
    @SerializedName("status")  var status: String,
    @SerializedName("totalResults")   val totalResults: Int,
    @SerializedName("articles") val newsList: ArrayList<NewsModel>
)