package ir.khosravi.kotlinews.viewModel

import android.app.Application
import android.view.View
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.ViewModel
import ir.khosravi.kotlinews.R
import ir.khosravi.kotlinews.model.NewsModel
import ir.khosravi.kotlinews.utils.ActivityUtils
import ir.khosravi.kotlinews.utils.App.Companion.context

class NewsDetailViewModel(application: Application) : AndroidViewModel(application) {

     lateinit var newsModel: NewsModel

    fun onClick(v: View) {
        if (v.id == R.id.btn_share && ::newsModel.isInitialized) {
            ActivityUtils.shareNewsDetail(getApplication() , newsModel)
        }
    }

}