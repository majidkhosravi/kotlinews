package ir.khosravi.kotlinews.viewModel

import android.accounts.NetworkErrorException
import android.view.View
import androidx.annotation.IntDef
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.paging.DataSource
import androidx.paging.LivePagedListBuilder
import androidx.paging.PageKeyedDataSource
import androidx.paging.PagedList
import ir.khosravi.kotlinews.R
import ir.khosravi.kotlinews.api.NetworkUtils
import ir.khosravi.kotlinews.controller.CategoryAdapter
import ir.khosravi.kotlinews.controller.paging.NewsListDataSource
import ir.khosravi.kotlinews.interfaces.OnItemClickListener
import ir.khosravi.kotlinews.model.NewsModel

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.cancel


/**
 * © Author Majid Khosravi since 09/21/2019.
 * https://gitlab.com/majidkhosravi
 * majid.khosravi89@gmail.com
 * https://www.farakav.com
 */


class MainViewModel : BaseViewModel() {

    private val dataSourceLiveData = MutableLiveData<PageKeyedDataSource<Int, NewsModel>>()
    private var itemPagedList: LiveData<PagedList<NewsModel>>
    private var mCategoryListAdapter: MutableLiveData<CategoryAdapter> = MutableLiveData()
    private var mBottomSheetState: MutableLiveData<Int> = MutableLiveData()
    private var mSelectedNewsModel: MutableLiveData<NewsModel> = MutableLiveData()
    private var categoryLiveDate: MutableLiveData<String> =
        MutableLiveData(NetworkUtils.Parameters.getCategoryNames()[0])



    val newsItemClickListener = object : OnItemClickListener {
        override fun onItemClick(model: Any?) {
            if (model != null && model is NewsModel) {
                mSelectedNewsModel.postValue(model)
                if (mBottomSheetState.value == BottomSheetState.EXPAND)
                    setBottomSheetState(BottomSheetState.HIDE)
            }

        }
    }

    private val mCategoryItemClickListener = object : OnItemClickListener {
        override fun onItemClick(model: Any?) {
            if (model != null && model is String) {

                categoryLiveDate.value = model
                if (dataSourceLiveData.value != null)
                    dataSourceLiveData.value!!.invalidate()

            }
            setBottomSheetState(BottomSheetState.HIDE)
        }
    }

    init {
        setBottomSheetState(BottomSheetState.HIDE)
        createCategoryListAdapter()
        itemPagedList = initialPagedList().build()
    }



    fun onClick(view: View) {
        if (view.id == R.id.btn_filter)
            when (mBottomSheetState.value) {
                BottomSheetState.HIDE -> setBottomSheetState(BottomSheetState.EXPAND)
                BottomSheetState.EXPAND -> setBottomSheetState(BottomSheetState.HIDE)
            }
    }


    fun getSelectedNewsModel(): LiveData<NewsModel> = mSelectedNewsModel

    fun getItemPagedList(): LiveData<PagedList<NewsModel>> = itemPagedList

    fun getCategoryListAdapter(): LiveData<CategoryAdapter> = mCategoryListAdapter

    fun getDefaultCategory(): LiveData<String> = categoryLiveDate


    fun getBottomSheetState(): LiveData<Int> = mBottomSheetState

    open class BottomSheetState {


        companion object {
            // The initial value is defined as the
            // 'BottomSheetBehavior.STATE_EXPANDED' and 'BottomSheetBehavior.STATE_HIDDEN'
            const val EXPAND = 3


            const val HIDE = 5


            @IntDef(EXPAND, HIDE)
            @Retention(AnnotationRetention.SOURCE)
            annotation class State
        }
    }

    private fun setBottomSheetState(@BottomSheetState.Companion.State state: Int) {
        mBottomSheetState.value = state
    }

    private fun createCategoryListAdapter() {
        val categoryAdapter = CategoryAdapter(NetworkUtils.Parameters.getCategoryNames())
        categoryAdapter.itemClickListener = mCategoryItemClickListener
        mCategoryListAdapter.postValue(categoryAdapter)
    }

    private fun initialPagedList(): LivePagedListBuilder<Int, NewsModel> {
        val config: PagedList.Config = PagedList.Config.Builder()
            .setEnablePlaceholders(false)
            .setPageSize(NetworkUtils.DEFAULT_PAGE_SIZE)
            .build()


        return LivePagedListBuilder<Int, NewsModel>(getDataSourceFactory(), config)
    }

    private fun getDataSourceFactory(): DataSource.Factory<Int, NewsModel> {
        return object : DataSource.Factory<Int, NewsModel>() {
            override fun create(): DataSource<Int, NewsModel> {
                val dataSource = NewsListDataSource(coroutineScope, getCategory())
                dataSourceLiveData.postValue(dataSource)
                return dataSource
            }
        }

    }

    private fun getCategory(): String {
        var category = ""
        if (categoryLiveDate.value != null) {
            category = categoryLiveDate.value!!
            if (category == "All") category = ""
        }
        return category
    }

}