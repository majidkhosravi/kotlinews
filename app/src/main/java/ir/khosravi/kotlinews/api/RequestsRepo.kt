package ir.khosravi.kotlinews.api

import android.util.Log
import com.google.gson.Gson
import ir.khosravi.kotlinews.model.ErrorModel
import ir.khosravi.kotlinews.model.ResponseModel
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * © Author Majid Khosravi since 09/21/2019.
 * https://gitlab.com/majidkhosravi
 * majid.khosravi89@gmail.com
 * https://www.farakav.com
 */

class RequestsRepo {

    companion object {
        lateinit var mListener: ResponseListener

        private object mCallback : Callback<ResponseModel> {
            override fun onResponse(call: Call<ResponseModel>, response: Response<ResponseModel>) {
                Log.i("The request was ", "${response.code()} ${"\t\t"} ${call.request().url()}")
                if (::mListener.isInitialized) {
                    if (response.code() == 200) {
                        mListener.OnResponse(response.body()!!)
                    } else {
                        val errorModel: ErrorModel =
                            Gson().fromJson(response.body().toString(), ErrorModel::class.java)
                        mListener.OnError(errorModel)
                    }
                }
            }

            override fun onFailure(call: Call<ResponseModel>, t: Throwable) {
                if (::mListener.isInitialized) {
                    val errorModel = ErrorModel("", t.cause.toString(), t.message)
                    mListener.OnError(errorModel)
                }
            }
        }

        fun getNews(category: String?, page: Int, listener: ResponseListener) {
            mListener = listener
            val call: Call<ResponseModel> =
                getApiInterface().getNews(
                    category = category!!,
                    pageSize = NetworkUtils.DEFAULT_PAGE_SIZE,
                    page = page
                )
            call.enqueue(mCallback)
        }

        internal fun getApiInterface(): ApiInterface {
            return ApiClient.getRetrofit().create(ApiInterface::class.java)
        }
    }


}
