package ir.khosravi.kotlinews.api

/**
 * © Author Majid Khosravi since 09/21/2019.
 * https://gitlab.com/majidkhosravi
 * majid.khosravi89@gmail.com
 * https://www.farakav.com
 */


abstract class NetworkUtils {
    companion object {
        public const val DEFAULT_PAGE_SIZE: Int = 20
        internal const val baseUrl: String = "https://newsapi.org/v2/"
        private const val and: String = "&"
        private const val exampleUrl: String =
            "top-headlines?country=us&category={selected_category}&page={page_number}"
        private const val newsUrl: String =
            Parameters.topHeadlines + and +
                    Parameters.country + and +
                    Parameters.pageSize + and +
                    Parameters.page

        fun getNewsUrl(category: String?, page: Int?): String {
            val url: String = String.format(
                newsUrl,
                Parameters.defaultCountry,
                DEFAULT_PAGE_SIZE,
                page
            )
            if (category.isNullOrEmpty() || category == Parameters.getCategoryNames().get(0)) return url
            else return url + and + String.format(Parameters.category, category)
        }
    }

    class Parameters {
        companion object {
            const val topHeadlines: String = "top-headlines?"
            const val apiKey: String = "apiKey=%s"
            const val country: String = "country=%s"
            const val defaultCountry: String = "us"
            const val category: String = "category=%s"
            const val page: String = "page=%s"
            const val pageSize: String = "pageSize=%s"

            fun getCategoryNames(): ArrayList<String> {
                val categoryNames = ArrayList<String>()
                categoryNames.add("All")
                categoryNames.add("Business")
                categoryNames.add("Entertainment")
                categoryNames.add("General")
                categoryNames.add("Health")
                categoryNames.add("Science")
                categoryNames.add("Sports")
                categoryNames.add("Technology")
                return categoryNames
            }
        }
    }

    class Header {
        companion object {
            const val apiKeyName: String = "X-Api-Key"
//            const val apiKeyValue: String = "YOUR_API_KEY_TAKEN_OF_NEWSAPI" //-> http://newsapi.org
            const val apiKeyValue: String = "655081fd96574471957d48d64c20866f" //-> http://newsapi.org

        }
    }
}