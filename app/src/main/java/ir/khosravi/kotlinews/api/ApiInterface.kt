package ir.khosravi.kotlinews.api

import io.reactivex.Maybe
import io.reactivex.MaybeSource
import io.reactivex.Observable
import io.reactivex.Single
import ir.khosravi.kotlinews.model.ResponseModel
import kotlinx.coroutines.Deferred
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * © Author Majid Khosravi since 09/21/2019.
 * https://gitlab.com/majidkhosravi
 * majid.khosravi89@gmail.com
 * https://www.farakav.com
 */

interface ApiInterface {

    @GET("top-headlines")
    fun getNews(
        @Query("country") country: String = NetworkUtils.Parameters.defaultCountry,
        @Query("category") category: String,
        @Query("pageSize") pageSize: Int,
        @Query("page") page: Int
    ): Call<ResponseModel>


    @GET("top-headlines")
    fun getNewsAsync(
        @Query("country") country: String = NetworkUtils.Parameters.defaultCountry,
        @Query("category") category: String,
        @Query("pageSize") pageSize: Int = NetworkUtils.DEFAULT_PAGE_SIZE,
        @Query("page") page: Int
    ): Deferred<Response<ResponseModel>>

}