package ir.khosravi.kotlinews.api

import ir.khosravi.kotlinews.model.ErrorModel
import ir.khosravi.kotlinews.model.ResponseModel

interface ResponseListener {

    fun OnResponse(responseModel: ResponseModel)

    fun OnError(errorModel: ErrorModel)
}