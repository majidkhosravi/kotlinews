package ir.khosravi.kotlinews.api

import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

/**
 * © Author Majid Khosravi since 09/21/2019.
 * https://gitlab.com/majidkhosravi
 * majid.khosravi89@gmail.com
 * https://www.farakav.com
 */

abstract class ApiClient {
    companion object {
        private lateinit var mRetrofit: Retrofit

     /*   private var headerInterceptor = Interceptor { chain ->
            val builderHeader: Request.Builder = chain.request().newBuilder()
            builderHeader.addHeader(NetworkUtils.Header.apiKeyName, NetworkUtils.Header.apiKeyValue)
            chain.proceed(builderHeader.build())
        }*/

        private val headerInterceptor: Interceptor = Interceptor { chain ->
            val newRequest = chain.request().newBuilder()
                .addHeader(NetworkUtils.Header.apiKeyName, NetworkUtils.Header.apiKeyValue)
                .build()
            chain.proceed(newRequest)
        }

        private var builder = OkHttpClient.Builder()
            .connectTimeout(1, TimeUnit.MINUTES)
            .readTimeout(1, TimeUnit.MINUTES)
            .addInterceptor(headerInterceptor)

        private val client = builder.build()

        fun getRetrofit(baseUrl : String = NetworkUtils.baseUrl): Retrofit {
            if (!::mRetrofit.isInitialized) {
                mRetrofit = Retrofit.Builder()
                    .baseUrl(baseUrl)
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(CoroutineCallAdapterFactory())
                    .client(client)
                    .build()
            }
            return mRetrofit
        }
    }
}