package ir.khosravi.kotlinews.utils

import androidx.appcompat.widget.AppCompatImageView
import com.bumptech.glide.Glide
import ir.khosravi.kotlinews.R


/**
 * © Author Majid Khosravi since 09/21/2019.
 * https://gitlab.com/majidkhosravi
 * majid.khosravi89@gmail.com
 * https://www.farakav.com
 */

class ImageUtils {
    companion object {

        fun setImage( imageUrl: String, imageView: AppCompatImageView,
            placeholderRes: Int = R.drawable.bg_placeholder ) {
            Glide.with(imageView.context)
                .load(imageUrl)
                .placeholder(placeholderRes)
                .into(imageView)
        }
    }
}