package ir.khosravi.kotlinews.utils

import android.view.View
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import ir.khosravi.kotlinews.controller.BaseAdapter
import ir.khosravi.kotlinews.model.NewsModel

class CustomBindingAdapters {

    companion object {
        @JvmStatic
        @BindingAdapter("imageUrl")
        fun AppCompatImageView.setImageUrl(url: String?) {
            val imageView: AppCompatImageView = this
            if (url.isNullOrEmpty()) ImageUtils.setImage("", imageView)
            else ImageUtils.setImage(url, imageView)
        }

        @JvmStatic
        @BindingAdapter("authorVisibility")
        fun AppCompatTextView.setAuthorVisibility(newsModel: NewsModel?) {
            this.visibility = if (newsModel?.author.isNullOrEmpty()) View.GONE else View.VISIBLE
        }

        @JvmStatic
        @BindingAdapter("newsContent")
        fun AppCompatTextView.setNewsContent(newsModel: NewsModel?) {
            this.text =
                if (newsModel?.content.isNullOrEmpty()) newsModel?.description else newsModel?.content
        }

        @JvmStatic
        @BindingAdapter("newsListAdapter")
        fun RecyclerView.setNewsListAdapter(adapter: RecyclerView.Adapter<BaseAdapter.BaseViewHolder>?) {
            if (adapter != null) this.adapter = adapter
        }

        @JvmStatic
        @BindingAdapter("pagingListAdapter")
        fun RecyclerView.setPagingListAdapter(adapter: RecyclerView.Adapter<BaseAdapter.BaseViewHolder>?) {
            if (adapter != null) this.adapter = adapter
        }

        @JvmStatic
        @BindingAdapter("categoryListAdapter")
        fun RecyclerView.setCategoryListAdapter(adapter: RecyclerView.Adapter<BaseAdapter.BaseViewHolder>?) {
            if (adapter != null) this.adapter = adapter
        }

        @JvmStatic
        @BindingAdapter("newsDate")
        fun AppCompatTextView.setNewsDate(date: String?) {
            this.text = DateUtils.getDetailDate(date!!)
        }
    }
}