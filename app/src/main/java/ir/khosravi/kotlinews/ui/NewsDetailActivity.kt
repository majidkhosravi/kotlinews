package ir.khosravi.kotlinews.ui

import android.content.Intent
import androidx.lifecycle.ViewModelProviders
import ir.khosravi.kotlinews.R
import ir.khosravi.kotlinews.databinding.ActivityDetailNewsBinding
import ir.khosravi.kotlinews.model.NewsModel
import ir.khosravi.kotlinews.utils.ActivityUtils
import ir.khosravi.kotlinews.viewModel.NewsDetailViewModel


/**
 * © Author Majid Khosravi since 09/21/2019.
 * https://gitlab.com/majidkhosravi
 * majid.khosravi89@gmail.com
 * https://www.farakav.com
 */

class NewsDetailActivity : BaseActivity() {

    private var newsModel: NewsModel? = null

    override fun getLayoutResource(): Int = R.layout.activity_detail_news

    override fun gatherData(data: Intent?) {
        if (data != null) {
            newsModel = data.extras.getParcelable<NewsModel>(ActivityUtils.AN_OBJECT_PARCEL_NAME)
        }
    }

    override fun doOtherTask() {
        if (this.newsModel != null) {
            val viewModel = getViewModel()
            viewModel.newsModel = this.newsModel!!
            (mBinding as ActivityDetailNewsBinding).viewMdoel = viewModel
        }
    }

    override fun getViewModel(): NewsDetailViewModel =
        ViewModelProviders.of(this).get(NewsDetailViewModel::class.java)

}