package ir.khosravi.kotlinews.ui

import androidx.lifecycle.ViewModel
import android.content.Intent
import android.os.Bundle
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding

/**
 * © Author Majid Khosravi since 09/21/2019.
 * https://gitlab.com/majidkhosravi
 * majid.khosravi89@gmail.com
 * https://www.farakav.com
 */

abstract class BaseActivity() : AppCompatActivity() {

    protected val TAG: String = javaClass.simpleName
    protected lateinit var mBinding: ViewDataBinding

    override fun onCreate(savedInstanceState: Bundle?) {

        if (intent != null)
            gatherData(intent)

        super.onCreate(savedInstanceState)
        setContentView(getLayoutResource())

        mBinding = DataBindingUtil.setContentView(this, getLayoutResource())
        mBinding.lifecycleOwner = this

        doOtherTask()
    }

    @LayoutRes
    protected abstract fun getLayoutResource(): Int

    protected open fun gatherData(data: Intent?) {}

    protected abstract fun doOtherTask()

    protected abstract fun getViewModel(): ViewModel
}