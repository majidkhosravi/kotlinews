package ir.khosravi.kotlinews.ui

import android.content.Intent
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.paging.PagedList
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.google.android.material.bottomsheet.BottomSheetBehavior
import ir.khosravi.kotlinews.R
import ir.khosravi.kotlinews.controller.paging.NewsPagingAdapter
import ir.khosravi.kotlinews.databinding.ActivityMainBinding
import ir.khosravi.kotlinews.model.NewsModel
import ir.khosravi.kotlinews.utils.ActivityUtils
import ir.khosravi.kotlinews.viewModel.MainViewModel
import kotlinx.android.synthetic.main.activity_main.main_bottom_sheet
import kotlinx.android.synthetic.main.main_bottom_sheet.*
import kotlinx.android.synthetic.main.main_content.*

class MainActivity : BaseActivity() {

    private var mBottomSheetBehavior: BottomSheetBehavior<*>? = null
    private val mSelectedNewsModelObserver = Observer<NewsModel> { newsModel ->
        val intent = Intent(this@MainActivity, NewsDetailActivity::class.java)
        intent.putExtra(ActivityUtils.AN_OBJECT_PARCEL_NAME, newsModel)
        startActivity(intent)
    }
    private val mBottomSheetStateObserver = Observer<Int> { state ->
        mBottomSheetBehavior?.state = state
    }

    override fun getLayoutResource() = R.layout.activity_main

    override fun doOtherTask() {
        mBottomSheetBehavior = BottomSheetBehavior.from(main_bottom_sheet)

        rv_category.layoutManager = LinearLayoutManager(this)

        rv_news.layoutManager = StaggeredGridLayoutManager(ActivityUtils.NEWS_LIST_SPAN_COUNT, StaggeredGridLayoutManager.VERTICAL)
        rv_news.setHasFixedSize(true)

        val viewModel = getViewModel()

        (mBinding as ActivityMainBinding).mainToolbar.viewModel = viewModel
        (mBinding as ActivityMainBinding).mainBottomSheet.viewModel = viewModel


        val adapter = NewsPagingAdapter()
        viewModel.getItemPagedList()
            .observe(this, Observer<PagedList<NewsModel>>(adapter::submitList))
        rv_news.adapter = adapter
        adapter.itemClickListener = viewModel.newsItemClickListener


        viewModel.getBottomSheetState().observe(this, mBottomSheetStateObserver)
        viewModel.getSelectedNewsModel().observe(this, mSelectedNewsModelObserver)

    }

    override fun getViewModel(): MainViewModel =
        ViewModelProviders.of(this).get(MainViewModel::class.java)


}