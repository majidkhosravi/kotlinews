package ir.khosravi.kotlinews.interfaces

/**
 * © Author Majid Khosravi since 09/21/2019.
 * https://gitlab.com/majidkhosravi
 * majid.khosravi89@gmail.com
 * https://www.farakav.com
 */

interface OnItemClickListener {
     fun onItemClick(model: Any?)
}